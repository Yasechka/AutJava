package servlet.us;

import dto.UserDTO;
import service.impl.UserServiceImpl;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(name = "RegServlet", urlPatterns = "/doRegister")
public class RegServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private UserServiceImpl userService;

    public RegServlet() {
        super();
        userService = new UserServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/reg.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        UserDTO user = new UserDTO();
        String name = (String) request.getParameter("firstName");
        String surname = (String) request.getParameter("secondName");
        String login = (String) request.getParameter("login");
        String password = (String) request.getParameter("password");
        String email = (String) request.getParameter("email");
        String gender = (String) request.getParameter("sex");
        String role = "USER";
        Date birthday = null;
        try {
            birthday = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("birthday"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        user = new UserDTO(name, surname, birthday, login, password, email, gender, role);
        String errorString = null;

        if (name == null || name =="" ||surname == null || surname =="" || login == null || login =="" ||
                password == null || password =="" || email == null || email =="" || gender == null || gender =="") {
            errorString = "User are invalid! Try again";
        }
        if (birthday==null){
            errorString="Error format of date";
        }

        if (errorString == null) {
            try {
                userService.create(user);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        request.setAttribute("errorString", errorString);
        request.setAttribute("user", user);

        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/reg.jsp");
            dispatcher.forward(request, response);
        }

        else {
            response.sendRedirect(request.getContextPath() + "/doLogin");
        }
    }
}
