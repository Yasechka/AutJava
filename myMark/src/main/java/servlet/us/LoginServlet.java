package servlet.us;

import dto.UserDTO;
import service.impl.UserServiceImpl;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "doLoginServlet", urlPatterns = { "/doLogin" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private UserServiceImpl userService;

    public LoginServlet() {
        super();
        userService = new UserServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String rememberMeStr = request.getParameter("rememberMe");
        boolean remember= "Y".equals(rememberMeStr);

        UserDTO user = null;
        boolean hasError = false;
        String errorString = null;

        if (login == null || password == null
                || login.length() == 0 || password.length() == 0) {
            hasError = true;
            errorString = "Input login and password!";
        } else {
            Connection conn = MyUtils.getStoredConnection(request);
            try {
                user = userService.findByLoginAndPassword(login,password);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (user == null) {
                hasError = true;
                errorString = "login or password invalid";
            }
        }

        if (hasError) {
            user = new UserDTO();
            user.setLogin(login);
            user.setPassword(password);

            request.setAttribute("errorString", errorString);
            request.setAttribute("user", user);

            RequestDispatcher dispatcher
                    = this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");

            dispatcher.forward(request, response);
        }

        else {
            HttpSession session = request.getSession();
            MyUtils.storeLoginedUser(session, user);
            String s = user.getRole();

            if(remember)  {
                MyUtils.storeUserCookie(response,user);
            }

            else  {
                MyUtils.deleteUserCookie(response);
            }

            response.sendRedirect(request.getContextPath() + "/userInfo");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}