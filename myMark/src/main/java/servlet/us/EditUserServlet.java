package servlet.us;

import dto.UserDTO;
import service.impl.UserServiceImpl;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(name = "EditUserServlet", urlPatterns = { "/editUser" })
public class EditUserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private UserServiceImpl userService;

    public EditUserServlet() {
        super();
        userService = new UserServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        String idd = (String) request.getParameter("id");
        int id = 0;
        try {
            id = Integer.parseInt(idd);
        }catch (Exception e){
        }
        UserDTO user = null;
        HttpSession session = request.getSession();
        UserDTO loginedUser = MyUtils.getLoginedUser(session);
        if (loginedUser == null) {
            response.sendRedirect(request.getContextPath() + "/doLogin");
            return;
        }
        else {
            try {
                user = userService.findById(id);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            request.setAttribute("user", user);
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/editUser.jsp");
            dispatcher.forward(request, response);}
//        else if (loginedUser.getRole().equals("USER")){
//            response.sendRedirect(request.getContextPath() + "/productList");}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        String idd = (String) request.getParameter("id");
        String name = (String) request.getParameter("firstName");
        String surname = (String) request.getParameter("secondName");
        String login = (String) request.getParameter("login");
        String password = (String) request.getParameter("password");
        String email = (String) request.getParameter("email");
        Date birthday = null;
        try {
            birthday = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("birthday"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int id = 0;
        try {
            id = Integer.parseInt(idd);
        }catch (Exception e){
        }
        UserDTO user = null;
        try {
            user = userService.findById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String errorString = null;
        if (name!=""&&surname!=""&&login!=""&&password!=""&&email!=""){
            user.setFirstName(name);
            user.setSecondName(surname);
            user.setLogin(login);
            user.setPassword(password);
            user.setEmail(email);
            user.setBirthday(birthday);
            try {
                userService.update(id,user);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else errorString = "oshibka";

        request.setAttribute("errorString", errorString);
        request.setAttribute("user", user);
        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/editUser.jsp");
            dispatcher.forward(request, response);
        }
        else {
            response.sendRedirect(request.getContextPath() + "/userInfo");
        }
    }
}