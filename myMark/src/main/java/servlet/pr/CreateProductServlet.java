package servlet.pr;

import dto.ProductDTO;
import dto.UserDTO;
import service.impl.ProductServiceImpl;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "CreateProductServlet",urlPatterns = { "/admin/createProduct" })
public class CreateProductServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private ProductServiceImpl productService;

    public CreateProductServlet() {
        super();
        productService = new ProductServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        UserDTO loginedUser = MyUtils.getLoginedUser(session);
        if (loginedUser == null) {
            response.sendRedirect(request.getContextPath() + "/doLogin");
            return;
        }
        else if (loginedUser.getRole().equals("ADMIN")){
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/admin/createProductView.jsp");
            dispatcher.forward(request, response);}
        else if (loginedUser.getRole().equals("USER")){
            response.sendRedirect(request.getContextPath() + "/productList");}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Connection conn = MyUtils.getStoredConnection(request);

        String title = (String) request.getParameter("title");
        String description = (String) request.getParameter("description");
        String countStr = (String) request.getParameter("count");
        String priceStr = (String) request.getParameter("price");
        int count = 0;
        try {
            count = Integer.parseInt(countStr);
        } catch (Exception e) {
        }
        float price = 0;
        try {
            price = Float.parseFloat(priceStr);
        } catch (Exception e) {
        }
        ProductDTO product = new ProductDTO(title,description, price, count);
        String errorString = null;

        if (description == null || description==""|| title==null || title=="") {
            errorString = "Product invalid!";
        }

        if (errorString == null) {
            try {
                productService.create(product);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        request.setAttribute("errorString", errorString);
        request.setAttribute("product", product);

        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/admin/createProductView.jsp");
            dispatcher.forward(request, response);
        }

        else {
            response.sendRedirect("/productList");
        }
    }

}
