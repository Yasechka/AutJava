package servlet.pr;

import dto.ProductDTO;
import dto.UserDTO;
import service.impl.ProductServiceImpl;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "DeleteProductServlet", urlPatterns = { "/admin/deleteProduct" })
public class DeleteProductServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private ProductServiceImpl productService;

    public DeleteProductServlet() {
        super();
        productService = new ProductServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        UserDTO loginedUser = MyUtils.getLoginedUser(session);
        Connection conn = MyUtils.getStoredConnection(request);
        String idd = (String) request.getParameter("id");
        int id = 0;
        try {
            id = Integer.parseInt(idd);
        }catch (Exception e){
        }
        ProductDTO product = null;
        if (loginedUser == null){
            response.sendRedirect(request.getContextPath() + "/doLogin");
            return;}
        else if (loginedUser.getRole().equals("ADMIN")){
            try {
                product = productService.findByID(id);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            request.setAttribute("product", product);
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/admin/deleteProductView.jsp");
            dispatcher.forward(request, response);}
        else if (loginedUser.getRole().equals("USER")){
            response.sendRedirect(request.getContextPath() + "/productList");}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        String idd = (String) request.getParameter("id");
        int id = 0;
        try {
            id = Integer.parseInt(idd);
        }catch (Exception e){
        }
        ProductDTO product = null;
        try {
            product = productService.findByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String errorString = null;
        try {
            productService.delete(id);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        if (errorString != null) {
            request.setAttribute("errorString", errorString);
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/admin/deleteProductView.jsp");
            dispatcher.forward(request, response);}
        else {
            response.sendRedirect(request.getContextPath() + "/productList");}
    }
}