package servlet.pr;

import dto.ProductDTO;
import dto.UserDTO;
import service.impl.ProductServiceImpl;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "EditProductServlet", urlPatterns = { "/admin/editProduct" })
public class EditProductServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private ProductServiceImpl productService;

    public EditProductServlet() {
        super();
        productService = new ProductServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        String idd = (String) request.getParameter("id");
        int id = 0;
        try {
            id = Integer.parseInt(idd);
        }catch (Exception e){
        }
        ProductDTO product = null;
        HttpSession session = request.getSession();
        UserDTO loginedUser = MyUtils.getLoginedUser(session);
        if (loginedUser == null) {
            response.sendRedirect(request.getContextPath() + "/doLogin");
            return;
        }
        else if (loginedUser.getRole().equals("ADMIN")){
            try {
                product = productService.findByID(id);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            request.setAttribute("product", product);
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/admin/editProductView.jsp");
            dispatcher.forward(request, response);}
        else if (loginedUser.getRole().equals("USER")){
            response.sendRedirect(request.getContextPath() + "/productList");}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        String idd = (String) request.getParameter("id");
        String title = (String) request.getParameter("title");
        String description = (String) request.getParameter("description");
        String countStr = (String) request.getParameter("count");
        String priceStr = (String) request.getParameter("price");
        int id = 0;
        try {
            id = Integer.parseInt(idd);
        }catch (Exception e){
        }
        int count = 0;
        try {
            count = Integer.parseInt(countStr);
        } catch (Exception e) {
        }
        float price = 0;
        try {
            price = Float.parseFloat(priceStr);
        } catch (Exception e) {
        }
        ProductDTO product = null;
        try {
            product = productService.findByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String errorString = null;
        if (title!=""&&description!=""){
            product.setTitle(title);
            product.setPrice(price);
            product.setDescription(description);
            product.setCount(count);
            try {
                productService.update(product);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else errorString = "oshibka";

        request.setAttribute("errorString", errorString);
        request.setAttribute("product", product);
        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/admin/editProductView.jsp");
            dispatcher.forward(request, response);
        }
        else {
            response.sendRedirect(request.getContextPath() + "/productList");
        }
    }
}