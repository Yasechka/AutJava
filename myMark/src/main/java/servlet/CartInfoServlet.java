package servlet;

import dto.ProductDTO;
import dto.UserDTO;
import service.impl.ProductServiceImpl;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.LinkedList;
import java.util.List;

@WebServlet(name = "CartInfoServlet", urlPatterns = "/korzina")
public class CartInfoServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private ProductServiceImpl productService;
    private BuyServlet buyServlet;

    public CartInfoServlet() {
        super();
        productService = new ProductServiceImpl();
        buyServlet = new BuyServlet();
    }

    static List<ProductDTO> productList = new LinkedList<>();

    public List<ProductDTO> getProducts(){

        return productList;
    }

    public long getAmount(){
        long sum = 0;
        for (ProductDTO product: productList) {
            sum+=product.getPrice();
        }
        return sum;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        PrintWriter writer = response.getWriter();
        String errorString = null;
        HttpSession session = request.getSession();
        UserDTO loginedUser = MyUtils.getLoginedUser(session);
        if (loginedUser == null) {
            response.sendRedirect(request.getContextPath() + "/doLogin");
            return;
        }
        if (session.getAttribute("cart")!=null){
             this.productList =  (List<ProductDTO>) session.getAttribute("cart");
            long amount = this.getAmount();
            session.setAttribute("amountA",amount);
        }
        else {
            this.productList = null;
            errorString = "Cart is empty";
        }
        request.setAttribute("errorString", errorString);

        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/cart.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
