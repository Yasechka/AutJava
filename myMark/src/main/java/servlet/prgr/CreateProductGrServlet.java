package servlet.prgr;

import dto.ProductGroupDTO;
import dto.UserDTO;
import service.impl.ProductGroupServiceImpl;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "CreateProductGrServlet",urlPatterns = { "/admin/createProductGr" })
public class CreateProductGrServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private ProductGroupServiceImpl productGroupService;

    public CreateProductGrServlet() {
        super();
        productGroupService = new ProductGroupServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        UserDTO loginedUser = MyUtils.getLoginedUser(session);
        if (loginedUser == null) {
            response.sendRedirect(request.getContextPath() + "/doLogin");
            return;
        }
        else if (loginedUser.getRole().equals("ADMIN")){
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/admin/createProductGrView.jsp");
            dispatcher.forward(request, response);}
        else if (loginedUser.getRole().equals("USER")){
            response.sendRedirect(request.getContextPath() + "/productGrList");}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Connection conn = MyUtils.getStoredConnection(request);

        String title = (String) request.getParameter("title");
        String description = (String) request.getParameter("description");

        ProductGroupDTO productgroup = new ProductGroupDTO(title,description);
        String errorString = null;

        if (description == null || description==""|| title==null || title=="") {
            errorString = "ProductGroup invalid!";
        }
        if (errorString == null) {
            try {
                productGroupService.create(productgroup);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        request.setAttribute("errorString", errorString);
        request.setAttribute("productgroup", productgroup);

        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/createProductGrView.jsp");
            dispatcher.forward(request, response);
        }
        else {
            response.sendRedirect("/productGrList");
        }
    }

}
