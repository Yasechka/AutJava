package servlet.prgr;

import dto.ProductGroupDTO;
import dto.UserDTO;
import service.impl.ProductGroupServiceImpl;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "DeleteProductGrServlet", urlPatterns = { "/admin/deleteProductGr" })
public class DeleteProductGrServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private ProductGroupServiceImpl productGroupService;

    public DeleteProductGrServlet() {
        super();
        productGroupService = new ProductGroupServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        UserDTO loginedUser = MyUtils.getLoginedUser(session);
        Connection conn = MyUtils.getStoredConnection(request);
        String idd = (String) request.getParameter("id");
        int id = 0;
        try {
            id = Integer.parseInt(idd);
        }catch (Exception e){
        }
        ProductGroupDTO productgroup = null;
        if (loginedUser == null){
            response.sendRedirect(request.getContextPath() + "/doLogin");
            return;}
        else if (loginedUser.getRole().equals("ADMIN")){
            try {
                productgroup = productGroupService.findByID(id);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            request.setAttribute("productgroup", productgroup);
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/admin/deleteProductGrView.jsp");
            dispatcher.forward(request, response);}
        else if (loginedUser.getRole().equals("USER")){
            response.sendRedirect(request.getContextPath() + "/productGrList");}

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        String idd = (String) request.getParameter("id");
        int id = 0;
        try {
            id = Integer.parseInt(idd);
        }catch (Exception e){
        }
        ProductGroupDTO product = null;
        try {
            product = productGroupService.findByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String errorString = null;
        try {
            productGroupService.delete(id);
        } catch (SQLException e) {
            e.printStackTrace();
            errorString = e.getMessage();
        }
        if (errorString != null) {
            request.setAttribute("errorString", errorString);
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/admin/deleteProductGrView.jsp");
            dispatcher.forward(request, response);}
        else {
            response.sendRedirect(request.getContextPath() + "/productGrList");}
    }
}