package servlet.prgr;

import dto.ProductGroupDTO;
import dto.UserDTO;
import service.impl.ProductGroupServiceImpl;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "EditProductGrServlet", urlPatterns = { "/admin/editProductGr" })
public class EditProductGrServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private ProductGroupServiceImpl productGroupService;

    public EditProductGrServlet() {
        super();
        productGroupService = new ProductGroupServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        String idd = (String) request.getParameter("id");
        int id = 0;
        try {
            id = Integer.parseInt(idd);
        }catch (Exception e){
        }
        ProductGroupDTO productgroup = null;
        HttpSession session = request.getSession();
        UserDTO loginedUser = MyUtils.getLoginedUser(session);
        if (loginedUser == null) {
            response.sendRedirect(request.getContextPath() + "/doLogin");
            return;
        }
        else if (loginedUser.getRole().equals("ADMIN")){
            try {
                productgroup = productGroupService.findByID(id);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            request.setAttribute("productgroup", productgroup);
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/admin/editProductGrView.jsp");
            dispatcher.forward(request, response);}
        else if (loginedUser.getRole().equals("USER")){
            response.sendRedirect(request.getContextPath() + "/productGrList");}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        PrintWriter out = response.getWriter();
        String idd = (String) request.getParameter("id");
        String title = (String) request.getParameter("title");
        String description = (String) request.getParameter("description");
        int id = 0;
        try {
            id = Integer.parseInt(idd);
        }catch (Exception e){
        }
        ProductGroupDTO productgroup = null;
        try {
            productgroup = productGroupService.findByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String errorString = null;
        if (title!=""&&description!=""){
            productgroup.setTitle(title);
            productgroup.setDescription(description);
            try {
                productGroupService.update(id,productgroup);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else errorString = "oshibka";

        request.setAttribute("errorString", errorString);
        request.setAttribute("productgroup", productgroup);
        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/admin/editProductGrView.jsp");
            dispatcher.forward(request, response);
        }
        else {
            response.sendRedirect(request.getContextPath() + "/productGrList");
        }
    }
}