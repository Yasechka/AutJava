package servlet.prgr;

import dto.ProductGroupDTO;
import dto.UserDTO;
import service.impl.ProductGroupServiceImpl;
import utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = { "/productGrList" })
public class ProductGrListServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private ProductGroupServiceImpl productGroupService;

    public ProductGrListServlet() {
        super();
        productGroupService = new ProductGroupServiceImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
        String errorString = null;
        List<ProductGroupDTO> list = null;
        HttpSession session = request.getSession();
        UserDTO loginedUser = MyUtils.getLoginedUser(session);
        if (loginedUser == null) {
            response.sendRedirect(request.getContextPath() + "/doLogin");
            return;
        }
        try {
            list = productGroupService.getAllProductDTO();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("errorString", errorString);
        request.setAttribute("productGroupList", list);
        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/productGrListView.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}