package servlet;

import dto.ProductDTO;
import service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

@WebServlet(name = "BuyServlet", urlPatterns = "/buyPr")
public class BuyServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private ProductServiceImpl productService;
    static List<ProductDTO> productList = new LinkedList<>();

    public BuyServlet() {
        super();
        productService = new ProductServiceImpl();
    }

    public void addProduct(ProductDTO e){

        productList.add(e);
    }

    public List<ProductDTO> getProducts(){

        return productList;
    }

//    public void removeFromCart(Product product) {
//        productList.remove(product);
//    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String idd = (String) request.getParameter("id");
        int id = 0;
        try {
            id = Integer.parseInt(idd);
        }catch (Exception e){
        }
        HttpSession session = request.getSession(true);

//        Connection conn = MyUtils.getStoredConnection(request);
            ProductDTO product = new ProductDTO();
            try {
                product = productService.findByID(id);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            product.setCount(1);
            this.addProduct(product);
            session.setAttribute("cart", this.getProducts());
            System.out.println(this.getProducts().size());
        response.sendRedirect(request.getContextPath() + "/korzina");
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request,response);
    }
}
