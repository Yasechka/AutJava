package datasource;

import entity.Product;
import entity.ProductGroup;
import entity.User;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
public class MySQL  implements InMemoryDB{

//	private String userName = null;
//	private String password = null;
//	private String serverName = null;
//	private  int portNumber = 3306;
//	private  String dbName = "test1";

	public  MySQL(){
	}

	//-----

	public  void inicDB(){
		PreparedStatement preparedStatement = null;
		boolean result= true;
		try{

			Connection connection = getConnection();
			String stroka =	"CREATE DATABASE test1";
			preparedStatement = connection.prepareStatement(stroka);
			preparedStatement.executeUpdate();
			connection.close();
		} catch (Exception e){
			e.printStackTrace();
			result= false;
		}
		try{
			Connection connection = getConnection();

			String stroka = "CREATE TABLE IF NOT EXISTS `product` ("
					+"`id` int(11) NOT NULL AUTO_INCREMENT,"
					+" `Description` text NOT NULL,"
					+" `count` int(11) NOT NULL,"
					+" `price` double NOT NULL,"
					+" `title` text NOT NULL,"
					+" PRIMARY KEY (`id`)"
					+") ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;";

			preparedStatement = connection.prepareStatement(stroka);
			preparedStatement.executeUpdate();

			stroka= "INSERT INTO `product` (`id`, `Description`, `count`, `price`, `title`) VALUES"
					+	"(1, 'desc1', 1, 22, 'title1'),"
					+	"(2, 'desc2', 10, 13, 'title2'),"
					+	"(3, 'desc3', 7, 21, 'title3'),"
					+	"(4, 'desc4', 4, 10, 'title4'),"
					+	"(5, 'desc5', 5, 15, 'title5'),"
					+	"(6, 'desc6', 8, 21, 'title6'),"
					+	"(7, 'desc7', 6, 20, 'title7'),"
					+	"(8, 'desc8', 4, 8, 'title8'),"
					+	"(9, 'desc9', 7, 17, 'title9');";

			preparedStatement = connection.prepareStatement(stroka);
			preparedStatement.executeUpdate();

			stroka=	"CREATE TABLE IF NOT EXISTS `productgroup` ("
					+	"  `id` int(11) NOT NULL AUTO_INCREMENT,"
					+	"  `title` text NOT NULL,"
					+	"  `description` text NOT NULL,"
					+	"  PRIMARY KEY (`id`)"
					+	") ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;" ;

			preparedStatement = connection.prepareStatement(stroka);
			preparedStatement.executeUpdate();

			stroka= "INSERT INTO `productgroup` (`id`, `title`, `description`) VALUES"
					+	"(1, 'Group1', 'descr1'),"
					+	"(2, 'Group2', 'descr2'),"
					+	"(3, 'Group3', 'descr3'),"
					+	"(4, 'Group4', 'descr4'),"
					+	"(5, 'Group5', 'descr5'),"
					+	"(6, 'Group6', 'descr6'),"
					+	"(7, 'Group7', 'descr7'),"
					+	"(8, 'Group8', 'descr8'),"
					+	"(9, 'Group9', 'descr9');";

			preparedStatement = connection.prepareStatement(stroka);
			preparedStatement.executeUpdate();

			stroka= "CREATE TABLE IF NOT EXISTS `users` ("
					+	"`id` int(11) NOT NULL AUTO_INCREMENT,"
					+	"`firstName` text NOT NULL,"
					+	"`secondName` text NOT NULL,"
					+	"`Birthday` text NOT NULL,"
					+	"`login` text NOT NULL,"
					+	"`password` text NOT NULL,"
					+	"`email` text NOT NULL,"
					+	"`sex` text NOT NULL,"
					+	"`role` text NOT NULL,"
					+	"PRIMARY KEY (`id`)"
					+	") ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;";

			preparedStatement = connection.prepareStatement(stroka);
			preparedStatement.executeUpdate();

			stroka=	"INSERT INTO `users` (`id`, `firstName`, `secondName`, `Birthday`, `login`, `password`, `email`, `sex`, `role`) VALUES"
					+	"(1, 'admin', 'admin', '1990-12-01', 'admin', 'admin', 'admin@email.com', 'MALE', 'ADMIN'),"
					+	"(2, 'user', 'user', '1995-02-11', 'user', 'user', 'user@email.com', 'FEMALE', 'USER');";

			preparedStatement = connection.prepareStatement(stroka);
			preparedStatement.executeUpdate();

			System.out.println("done");

		} catch (Exception e){
			e.printStackTrace();
			result= false;
		}
	}

	//--------

	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		String hostName = "localhost";
		String dbName = "test1";
		String userName = "root";
		String passwd = "root";
		return getMySQLConnection(hostName, dbName, userName, passwd);
	}

	public static Connection getMySQLConnection(String hostName, String dbName, String userName, String password)
			throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		String connectionURL = "jdbc:mysql://" + hostName + ":3306/" + dbName;
		Connection conn = DriverManager.getConnection(connectionURL, userName, password);
		return conn;
	}

	public static void closeConnection(Connection conn) {
		try {
			conn.close();
		} catch (Exception e) {
		}
	}

	public static void rollbackConnection(Connection conn) {
		try {
			conn.rollback();
		} catch (Exception e) {
		}
	}

	//------

	@Override
	public User addUser(User user) throws ClassNotFoundException {
		PreparedStatement preparedStatement = null;
		Connection connection;
		try {
			connection = getConnection();
			preparedStatement = connection.prepareStatement(
					"INSERT INTO users( firstName, secondName, Birthday, login,password,email, sex, role) values(?,?,?,?,?,?,?,?)");

			preparedStatement.setString(1, user.getFirstName());
			preparedStatement.setString(2, user.getSecondName());
			preparedStatement.setDate(3, new java.sql.Date(user.getBirthday().getTime()));
			preparedStatement.setString(4, user.getLogin());
			preparedStatement.setString(5, user.getPassword());
			preparedStatement.setString(6, user.getEmail());
			preparedStatement.setString(7, user.getSex());
			preparedStatement.setString(8, user.getRole());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public boolean deleteUser(Integer id) {
		boolean result = true;

		User u = new User();
		PreparedStatement preparedStatement = null;
		try {
			Connection connection = getConnection();
			preparedStatement = connection.prepareStatement("DELETE FROM users where id =?");

			preparedStatement.setInt(1, id);
			result = preparedStatement.execute();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}

	@Override
	public User updateUser(User user) throws ClassNotFoundException {
		try {
			PreparedStatement preparedStatement = null;
			Connection connection = getConnection();
			preparedStatement = connection.prepareStatement("UPDATE users SET firstName=?, secondName=?, Birthday=?, login=?,password=?,email=? WHERE id=?");

			preparedStatement.setInt(7, user.getId());
			preparedStatement.setString(1, user.getFirstName());
			preparedStatement.setString(2, user.getSecondName());
			preparedStatement.setDate(3, new java.sql.Date(user.getBirthday().getTime()));
			preparedStatement.setString(4, user.getLogin());
			preparedStatement.setString(5, user.getPassword());
			preparedStatement.setString(6, user.getEmail());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public User findUserByLoginAndPassword(String login, String password) {
		User u = new User();
		PreparedStatement preparedStatement = null;
		try {
			Connection connection = getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM users where login =? AND password =?");

			preparedStatement.setString(1, login);
			preparedStatement.setString(2, password);

			ResultSet res = preparedStatement.executeQuery();

			if (res.next()) {
				u.setEmail(res.getString("email"));
				u.setFirstName(res.getString("firstName"));
				u.setId(res.getInt("id"));
				u.setLogin(res.getString("login"));
				u.setPassword(res.getString("password"));
				u.setSecondName(res.getString("secondName"));
				u.setSex(res.getString("sex"));
				u.setRole(res.getString("role"));
				u.setBirthday(res.getDate("birthday"));
			}
			res.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return u;
	}

	@Override
	public User findUserByLoginAndEmail(String login) throws SQLException, ClassNotFoundException {
		PreparedStatement preparedStatement = null;
		User u = null;
		try {
			Connection connection = getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM users where login =?");

			preparedStatement.setString(1, login);

			ResultSet res = preparedStatement.executeQuery();

			if (res.next()) {
				u = new User();
				u.setEmail(res.getString("email"));
				u.setFirstName(res.getString("firstName"));
				u.setId(res.getInt("id"));
				u.setLogin(res.getString("login"));
				u.setPassword(res.getString("password"));
				u.setSecondName(res.getString("secondName"));
				u.setSex(res.getString("sex"));
				u.setRole(res.getString("role"));
				u.setBirthday(res.getDate("birthday"));
				return u;
			}
			res.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User findById(int id) {

		User u = new User();
		PreparedStatement preparedStatement = null;
		try {
			Connection connection = getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM users where id =? ");

			preparedStatement.setInt(1, id);

			ResultSet res = preparedStatement.executeQuery();
			if (res.next()) {
				u.setEmail(res.getString("email"));
				u.setFirstName(res.getString("firstName"));
				u.setId(res.getInt("id"));
				u.setLogin(res.getString("login"));
				u.setPassword(res.getString("password"));
				u.setSecondName(res.getString("secondName"));
				u.setSex(res.getString("sex"));
				u.setRole(res.getString("role"));
				u.setBirthday(res.getDate("birthday"));
			}
			res.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return u;
	}

	//-------

	@Override
	public boolean createProduct(Product product) throws ClassNotFoundException {
	boolean result = true;

	PreparedStatement preparedStatement = null;
	Connection connection;
	try {
		connection = getConnection();

		preparedStatement = connection.prepareStatement("INSERT INTO product( Description, count, price, title) values(?,?,?,?)");

		preparedStatement.setString(1, product.getDescription());
		preparedStatement.setInt(2, product.getCount());
		preparedStatement.setFloat(3, product.getPrice());
		preparedStatement.setString(4, product.getTitle());
		preparedStatement.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();
		result = false;
	}
	return result;
}

	@Override
	public Product updateProduct(Product product) throws ClassNotFoundException {
		try {

			PreparedStatement preparedStatement = null;
			Connection connection = getConnection();
			preparedStatement = connection
					.prepareStatement("UPDATE product SET Description=?, count=?, price=?, title=? WHERE id=?");

			preparedStatement.setString(1, product.getDescription());
			preparedStatement.setInt(2, product.getCount());
			preparedStatement.setDouble(3, product.getPrice());
			preparedStatement.setString(4, product.getTitle());
			preparedStatement.setInt(5, product.getId());

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return product;
	}

	@Override
	public boolean deleteProduct(Integer id) {
		boolean result = true;

		PreparedStatement preparedStatement = null;
		try {
			Connection connection = getConnection();
			preparedStatement = connection.prepareStatement("DELETE FROM product where id =?");

			preparedStatement.setInt(1, id);
			result = preparedStatement.execute();

			preparedStatement = connection.prepareStatement("DELETE FROM ppg where idP =?");

			preparedStatement.setInt(1, id);
			result = preparedStatement.execute();

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		System.out.println("delete product id: " + id + ":" + result);
		return result;
	}

	@Override
	public Product findProductById(int i) {
		Product newProduct = new Product();

		PreparedStatement preparedStatement = null;
		try {
			Connection connection = getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM product where id =? ");

			preparedStatement.setInt(1, i);

			ResultSet res = preparedStatement.executeQuery();
			if (res.next()) {

				newProduct.setDescription(res.getString("description"));
				newProduct.setTitle(res.getString("title"));
				newProduct.setId(res.getInt("id"));
				newProduct.setCount(res.getInt("count"));
				newProduct.setPrice(res.getFloat("price"));
			}
			res.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return newProduct;
	}

	@Override
	public List<Product> findAllProducts() {

		PreparedStatement preparedStatement = null;
		List<Product> newList = new LinkedList<>();
		try {
			Connection connection = getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM product");

			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {

				Product newProduct = new Product();
				newProduct.setDescription(res.getString("description"));
				newProduct.setTitle(res.getString("title"));
				newProduct.setId(res.getInt("id"));
				newProduct.setCount(res.getInt("count"));
				newProduct.setPrice(res.getFloat("price"));
				newList.add(newProduct);
			}
			res.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return newList;
	}

	//------

	public ProductGroup addProductGroup(ProductGroup pg) throws ClassNotFoundException {

		PreparedStatement preparedStatement = null;
		Connection connection;
		try {
			connection = getConnection();

			preparedStatement = connection
					.prepareStatement("INSERT INTO productgroup( title, description) values(?,?)");

			preparedStatement.setString(1, pg.getTitle());
			preparedStatement.setString(2, pg.getDescription());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pg;
	}

	@Override
	public boolean updateGroup(int i, ProductGroup productGroup) throws ClassNotFoundException {
		boolean res = true;

		try {
			PreparedStatement preparedStatement = null;
			Connection connection = getConnection();
			preparedStatement = connection.prepareStatement(

					"UPDATE productgroup SET title=?, description=? WHERE id=?");

			preparedStatement.setInt(3, i);
			preparedStatement.setString(1, productGroup.getTitle());
			preparedStatement.setString(2, productGroup.getDescription());

			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			res = false;
		}
		return res;
	}

	@Override
	public boolean deleteProductGroup(int id) {
		boolean result = true;

		PreparedStatement preparedStatement = null;
		try {
			Connection connection = getConnection();
			preparedStatement = connection.prepareStatement("DELETE FROM productgroup where id =?");

			preparedStatement.setInt(1, id);
			result = preparedStatement.execute();

			preparedStatement = connection.prepareStatement("DELETE FROM ppg where idPG =?");

			preparedStatement.setInt(1, id);
			result = preparedStatement.execute();

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		System.out.println("delete product Group: " + result);
		return result;
	}

	@Override
	public ProductGroup findProductGroupbyId(int id) {
		ProductGroup pg = new ProductGroup();
		PreparedStatement preparedStatement = null;
		try {
			Connection connection = getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM productgroup where id =? ");

			preparedStatement.setInt(1, id);

			ResultSet res = preparedStatement.executeQuery();
			if (res.next()) {

				pg.setDescription(res.getString("description"));
				pg.setTitle(res.getString("title"));
				pg.setId(res.getInt("id"));
			}
			res.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pg;
	}

	@Override
	public List<ProductGroup> getAllProductGroups() {
		List<ProductGroup> newList = new LinkedList<>();

		PreparedStatement preparedStatement = null;
		try {
			Connection connection = getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM productgroup ");

			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {
				ProductGroup pg = new ProductGroup();
				pg.setDescription(res.getString("description"));
				pg.setTitle(res.getString("title"));
				pg.setId(res.getInt("id"));
				newList.add(pg);
			}			res.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newList;
	}

}
