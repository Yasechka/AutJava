package datasource;

import entity.Product;
import entity.ProductGroup;
import entity.User;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class InMemoryDBImpl implements InMemoryDB {
	private List<User> users = new LinkedList<>();
	 private HashMap<Integer, ProductGroup> productGroups = new HashMap<>();
	 private static int count = 0;
	 private static int countPrGr = 0;

	//---
	@Override
	public synchronized User addUser(User user){
		User userResult = null;
		User unicUser = findUserByLoginAndPassword(user.getLogin(), user.getPassword());
		if (unicUser==null){
			user.setId(count +1);
			users.add(user);
			userResult = user;
			count++;
		}
		userResult = user;

		return userResult;
	}

	@Override
	public synchronized boolean deleteUser(Integer id) {
		User userResult = null;
		Boolean result =false;
		User unicUser = findById(id);
		if (unicUser!=null){
			users.remove(unicUser);
		}
		return result;
	}

	@Override
	public User updateUser(User user) {
		return null;
	}

	@Override
	public User findUserByLoginAndPassword(String login, String password){
		 User userResult = null;

		 for (User user :users){
			 if (user.getLogin().equals(login) && user.getPassword().equals(password)){
				userResult = user;
			 }
		 }
		return userResult;
	  }

	@Override
	public User findUserByLoginAndEmail(String login){
			User userResult = null;
			for (User user :users){
				if (user.getLogin().equals(login)){
					userResult = user;
				}
			}
			return userResult;
		}

	@Override
	public User findById(int id) {
	 User userResult = null;
		 for (User user :users){
			 if (user.getId().equals(id)) {
				userResult = user;
			 }
		 }
		return userResult;
	}

	//-----

	@Override
	public boolean createProduct(Product product) {
		boolean result= true;
		return result;
	}

	@Override
	public boolean deleteProduct(Integer id) {
		return false;
	}

	@Override
	public Product updateProduct(Product product) {
		return null;
	}

	@Override
	public Product findProductById(int i) {
		return null;
	}

	@Override
	public List<Product> findAllProducts() {
		return null;
	}

	private boolean ProductGroupExists(ProductGroup pg) {
		boolean result = false;
		return result;
	}

	//----

	public ProductGroup addProductGroup(ProductGroup productGroup) {
		boolean result = false;
		if (ProductGroupExists(productGroup) == false){
			result = true;
			countPrGr++;
			productGroup.setId(countPrGr);
			productGroups.put(countPrGr, productGroup);
		}
		return productGroup;
	}

	@Override
	public boolean deleteProductGroup(int id) {
		return false;
	}

	@Override
	public boolean updateGroup(int i, ProductGroup productGroup) {
		return false;
	}

	@Override
	public ProductGroup findProductGroupbyId(int id) {
		return null;
	}

	@Override
	public List<ProductGroup> getAllProductGroups() {
		return null;
	}

}
