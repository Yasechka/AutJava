package datasource;

import entity.Product;
import entity.ProductGroup;
import entity.User;

import java.sql.SQLException;
import java.util.List;

public interface InMemoryDB {

	//------user
	User addUser(User user) throws ClassNotFoundException;
	boolean deleteUser(Integer id);
	User updateUser(User user) throws ClassNotFoundException;
	User findById(int id);
	User findUserByLoginAndPassword(String login, String password);
	User findUserByLoginAndEmail(String login) throws SQLException, ClassNotFoundException;
	//------

	//------product
	boolean createProduct(Product product) throws ClassNotFoundException;
	boolean deleteProduct(Integer id);
	Product updateProduct(Product product) throws ClassNotFoundException;
	Product findProductById(int i);
	List<Product> findAllProducts();
	//------

	//------product gr
	ProductGroup addProductGroup(ProductGroup productGroup) throws ClassNotFoundException;
	boolean deleteProductGroup(int id);
	boolean updateGroup(int i, ProductGroup productGroup) throws ClassNotFoundException;
	ProductGroup findProductGroupbyId(int id);
	List<ProductGroup> getAllProductGroups();


	//------
//
}