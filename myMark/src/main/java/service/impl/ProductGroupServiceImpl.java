package service.impl;

import dao.impl.ProductGroupDAOImpl;
import dto.ProductGroupDTO;
import entity.ProductGroup;
import helper.Transformer;
import service.api.ProductGroupService;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ProductGroupServiceImpl implements ProductGroupService {

	private ProductGroupDAOImpl productDao = new ProductGroupDAOImpl();;

	@Override
	public ProductGroupDTO findByID(Integer Id) throws SQLException {
		ProductGroupDTO result = new ProductGroupDTO();
		ProductGroup pro = productDao.findById(Id);
		result = Transformer.transformGroupToGroupDT0(pro);
		return result;
	}

	@Override
	public ProductGroupDTO create(ProductGroupDTO product) throws ClassNotFoundException, SQLException {
		
		ProductGroup pro = null;
		pro = Transformer.transformGroupDToToGroup(product);
		
		productDao.create(pro);
		System.out.println("creating pg :" + pro.toString());
		return product;
		
	}

	@Override
	public boolean delete(Integer id) throws SQLException {
		boolean res = true;
		ProductGroupDTO result = new ProductGroupDTO();
		result = findByID(id);
		if (result ==null){
			res = false;
		}else{
			productDao.delete(id);
		}
		
		return res;
	}

	@Override
	public List<ProductGroupDTO> getAllProductDTO() throws SQLException {
		List<ProductGroupDTO> l = new LinkedList<>();
		List<ProductGroup> lPG = new LinkedList<>();
		lPG= productDao.getAllPGDTO();
		l = Transformer.TransformListPgtoPGDTO(lPG);
		return l;
	}

	@Override
	public boolean update(int i, ProductGroupDTO pg) throws ClassNotFoundException, SQLException {
		boolean result = true;
		 ProductGroupDTO oldProduct = findByID(i);
		 if (oldProduct!=null && pg!=null){
			 oldProduct.setTitle(pg.getTitle());
			 oldProduct.setDescription(pg.getDescription());
			 ProductGroup pgNew = Transformer.transformGroupDToToGroup(oldProduct);
			 productDao.update(i, pgNew);
			 }else{
			 result = false;
		 }
		return result;
	}

    @Override
    public List<ProductGroupDTO> getAllPPG() throws SQLException {
        List<ProductGroupDTO> l = new LinkedList<>();
        List<ProductGroup> lPG = new LinkedList<>();
        lPG= productDao.getAllPGDTO();
        l = Transformer.TransformListPgtoPGDTO(lPG);
        return l;
    }


}
