package service.impl;

import dao.api.UserDAO;
import dao.impl.UserDAOImpl;
import dto.UserDTO;
import entity.User;
import helper.Transformer;
import service.api.UserService;

import java.sql.SQLException;

public class UserServiceImpl implements UserService {

    private UserDAO userDAO = new UserDAOImpl();
    public int counter =0;
    @Override
    public UserDTO findByLoginAndPassword(String login, String password) throws SQLException {
        User user = userDAO.findByLoginAndPassword(login, password);
        System.out.println("find:"+ user.toString());
        UserDTO userDTO = Transformer.transformUserToUserDTO(user);
        if (userDTO == null){
        	userDTO = null;
        }
        return userDTO;
    }

    @Override
    public UserDTO findByLoginAndEmail(String login) throws SQLException, ClassNotFoundException {
        User user = userDAO.findByLoginAndEmail(login);
//        System.out.println("find:"+ user.toString());
        UserDTO userDTO = Transformer.transformUserToUserDTO(user);
        if (userDTO == null){
            userDTO = null;
        }
        return userDTO;
    }

    @Override
    public UserDTO create(User user) throws ClassNotFoundException, SQLException {
    	UserDTO u = Transformer.transformUserToUserDTO(user);
    	User userNew =userDAO.create(user);
    	
    	return u;
    }

    @Override
    public boolean delete(Integer id) throws SQLException {
        boolean res = userDAO.delete(id);
        return res;
    }

    @Override
    public UserDTO update( int i, UserDTO newUser) throws ClassNotFoundException, SQLException {
    	if (newUser!=null){
    	User u = Transformer.transformUserDTOToUser(newUser);
		userDAO.update(i, u);}
    	return newUser;
    }

	public UserDTO create(UserDTO user) throws ClassNotFoundException, SQLException {
		counter++;
		user.setId(counter);
		User u = Transformer.transformUserDTOToUser(user);
		User userNew =userDAO.create(u);
		return user;
	}

	@Override
	public UserDTO findById(int id) throws SQLException {
		UserDTO res = null;
		 User user = userDAO.FindById(id);
		 res = Transformer.transformUserToUserDTO(user);
		
		System.out.println("finding to update:" + res);
		return res;
	}
}
