package service.impl;

import dao.impl.ProductDAOImpl;
import dto.ProductDTO;
import entity.Product;
import helper.Transformer;
import service.api.ProductService;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ProductServiceImpl implements ProductService {
    private ProductGroupServiceImpl proGDTO = new ProductGroupServiceImpl ();
	private ProductDAOImpl productDao = new ProductDAOImpl();

	@Override
	public ProductDTO findByID(Integer Id) throws SQLException {
		ProductDTO result = new ProductDTO();
		Product pro = productDao.findById(Id);
		result = Transformer.transformProductToProductDTO(pro);
		return result;
	}

	@Override
	public ProductDTO create(ProductDTO productDTO) throws ClassNotFoundException, SQLException {
		Product product = new Product();
		product = Transformer.transformProductDTOToProduct(productDTO);
	    productDao.create(product);
		return productDTO;
	}

	@Override
	public boolean delete(Integer id) throws SQLException {
		Boolean res = true;
		Product product = new Product();
		product = productDao.findById(id);
		System.out.println("find before deleting=" + product );
		res= productDao.delete(id);
		return res;
	}

	@Override
	public ProductDTO update(ProductDTO productDTO) throws ClassNotFoundException, SQLException {
		Product product = new Product();
		int id = 0;
	    product = Transformer.transformProductDTOToProduct(productDTO);
	    id = product.getId();
		productDao.update(product);
	    return productDTO;
	}

	@Override
	public List<ProductDTO> getAllProductDTO() throws SQLException {
		List<ProductDTO> productDTO = new LinkedList<>();
		List<Product> products = new LinkedList<>();
		products= productDao.getProductList();
		productDTO = Transformer.ListProductToProductDto(products);
		return productDTO;
	}
}
