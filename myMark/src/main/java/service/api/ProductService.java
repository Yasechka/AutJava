package service.api;

import dto.ProductDTO;

import java.sql.SQLException;
import java.util.List;

public interface ProductService {

    ProductDTO findByID(Integer Id) throws SQLException;

    ProductDTO create(ProductDTO product) throws ClassNotFoundException, SQLException;

    boolean delete(Integer id) throws SQLException;

    ProductDTO update(ProductDTO product) throws ClassNotFoundException, SQLException;

	List<ProductDTO> getAllProductDTO() throws SQLException;
}
