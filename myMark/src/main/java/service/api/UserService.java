package service.api;

import dto.UserDTO;
import entity.User;

import java.sql.SQLException;

public interface UserService {

    UserDTO findByLoginAndPassword(String login, String password) throws SQLException;

    UserDTO findByLoginAndEmail(String login) throws SQLException, ClassNotFoundException;

    UserDTO create(UserDTO user) throws ClassNotFoundException, SQLException;

    boolean delete(Integer id) throws SQLException;

    UserDTO update(int i, UserDTO newUser) throws ClassNotFoundException, SQLException;

	UserDTO create(User user) throws ClassNotFoundException, SQLException;
     
    UserDTO findById(int id) throws SQLException;
}
