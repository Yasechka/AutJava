package service.api;

import dto.ProductGroupDTO;

import java.sql.SQLException;
import java.util.List;

public interface ProductGroupService {
	    ProductGroupDTO findByID(Integer Id) throws SQLException;
 
	    ProductGroupDTO create(ProductGroupDTO product) throws ClassNotFoundException, SQLException;

	    boolean delete(Integer id) throws SQLException;

		List<ProductGroupDTO> getAllProductDTO() throws SQLException;

		boolean update(int i, ProductGroupDTO pg) throws ClassNotFoundException, SQLException;

	List<ProductGroupDTO> getAllPPG() throws SQLException;
}
