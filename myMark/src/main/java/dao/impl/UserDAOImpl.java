package dao.impl;

import datasource.MySQL;
import dao.api.UserDAO;
import entity.User;

import java.sql.SQLException;

public class UserDAOImpl implements UserDAO{

	private MySQL inmem = new MySQL();

	@Override
	public User create(User user) throws ClassNotFoundException,SQLException {
		return inmem.addUser(user);
	}

	@Override
	public boolean delete(Integer id) throws SQLException  {
		return inmem.deleteUser(id);
	}

	@Override
	public User update(int i, User user) throws ClassNotFoundException,SQLException {

		if (inmem.findById(i) != null){
			user.setId(i);
			inmem.updateUser(user);
		}else
		{
			user= null;
		}
		return user;
	}

	@Override
	public User FindById(int id) throws SQLException {
		User user = null;
		user = inmem.findById(id);
		return user;
	}

	@Override
    public User findByLoginAndPassword(String login, String password) throws SQLException {
        User user = null;
        user = inmem.findUserByLoginAndPassword(login, password);
        return user;
    }

    @Override
    public User findByLoginAndEmail(String login) throws SQLException, ClassNotFoundException,SQLException {
		User user = null;
		user = inmem.findUserByLoginAndEmail(login);
		return user;
    }
}
