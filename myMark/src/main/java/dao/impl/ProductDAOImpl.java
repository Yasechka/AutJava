package dao.impl;

import datasource.InMemoryDB;
import datasource.MySQL;
import dao.api.ProductDAO;
import entity.Product;

import java.sql.SQLException;
import java.util.List;

public class ProductDAOImpl implements ProductDAO {

	static InMemoryDB inmem = new MySQL();

	@Override
    public Product create(Product product) throws ClassNotFoundException,SQLException {
    	inmem.createProduct(product);
    	return product;
    }

	@Override
	public boolean delete(Integer id) throws SQLException {
		return inmem.deleteProduct(id);
	}

    @Override
    public Product update(Product product) throws ClassNotFoundException,SQLException {
    	return inmem.updateProduct(product);
    }

	@Override
	public  Product findById(int i) throws SQLException  {
		return inmem.findProductById(i);
	}

	@Override
	public List<Product> getProductList() throws SQLException{
		List<Product> list = inmem.findAllProducts();
		return list ;
	}
}
