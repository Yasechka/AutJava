package dao.impl;

import datasource.InMemoryDB;
import datasource.MySQL;
import dao.api.ProductGroupDAO;
import entity.Product;
import entity.ProductGroup;

import java.sql.SQLException;
import java.util.List;

public class ProductGroupDAOImpl implements ProductGroupDAO {

	static InMemoryDB inmem = new MySQL();

	@Override
	public  ProductGroup create(ProductGroup productGroup) throws ClassNotFoundException, SQLException {
		inmem.addProductGroup(productGroup);
		return productGroup;
	}

	@Override
	public boolean delete(Integer id) throws SQLException {
		inmem.deleteProductGroup(id);
		return true;
	}

	@Override
	public ProductGroup update(int i, ProductGroup productGroup) throws ClassNotFoundException,SQLException {
		inmem.updateGroup(i, productGroup);
		return productGroup;
	}

	@Override
	public ProductGroup update(ProductGroup productGroup) throws SQLException {
		return null;
	}

	public ProductGroup findById(Integer id) throws SQLException {
		ProductGroup pg = null;
		pg = inmem.findProductGroupbyId(id);
		return pg;
	}

	@Override
	public List<Product> findProducts(Integer id) throws SQLException {
		return null;
	}

	public List<ProductGroup> getAllPGDTO() throws SQLException {
		return inmem.getAllProductGroups();
	}
}
