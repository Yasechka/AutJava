package dao.api;

import entity.Product;
import entity.ProductGroup;

import java.sql.SQLException;
import java.util.List;

public interface ProductGroupDAO {

    ProductGroup create(ProductGroup productGroup) throws ClassNotFoundException, SQLException;

    boolean delete(Integer id) throws SQLException;

    ProductGroup update(int i, ProductGroup productGroup) throws ClassNotFoundException, SQLException;

    ProductGroup update(ProductGroup productGroup) throws SQLException;

    List<Product> findProducts(Integer id) throws SQLException;
}
