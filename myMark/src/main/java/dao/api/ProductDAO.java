package dao.api;

import entity.Product;

import java.sql.SQLException;
import java.util.List;

public interface ProductDAO {

    Product create(Product product) throws ClassNotFoundException, SQLException;

    boolean delete(Integer id) throws SQLException;

    Product update(Product product) throws ClassNotFoundException, SQLException;

    Product findById(int i) throws SQLException;

    List<Product> getProductList() throws SQLException;
}
