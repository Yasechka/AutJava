package dao.api;

import entity.User;

import java.sql.SQLException;

public interface UserDAO {

    User create(User user) throws ClassNotFoundException, SQLException;

    boolean delete(Integer id) throws SQLException;

    User update(int i, User user) throws ClassNotFoundException, SQLException;

    User FindById(int id) throws SQLException;

    User findByLoginAndPassword(String login, String password) throws SQLException;

    User findByLoginAndEmail(String login) throws SQLException, ClassNotFoundException;
}
