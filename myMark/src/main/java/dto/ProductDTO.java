package dto;

public class ProductDTO {
	private Integer id;
	private String title;
	private String description;
	private Float price;
	private Integer count;

	public ProductDTO() {
	}

	public ProductDTO(String title, String description, Float price, Integer count) {
		this.title = title;
		this.description = description;
		this.price = price;
		this.count = count;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
}
