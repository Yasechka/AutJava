package dto;

import java.util.Date;

public class UserDTO {

    private Integer id;
    private String firstName;
    private String secondName;
    private Date birthday;
    private String login;
    private String password;
    private String email;
    private String sex;
    private String role;

    public UserDTO() {
    }

    public UserDTO(String firstName, String secondName, Date birthday, String login, String password, String email, String sex, String role) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.birthday = birthday;
        this.login = login;
        this.password = password;
        this.email = email;
        this.sex = sex;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}