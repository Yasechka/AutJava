<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>User Info</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="menu.jsp"></jsp:include>
<c:forEach items="${user}" var="user" >

<h3>Hello, ${user.login}</h3>
First name: ${user.firstName} <br />
Second name: ${user.secondName} <br />
Bithday: ${user.birthday} <br />
Email: ${user.email}<br />
Sex: ${user.sex} <br />
Role: ${user.role}<br />
<br /><a href="editUser?id=${user.id}">Edit</a>
</c:forEach>
</body>
</html>