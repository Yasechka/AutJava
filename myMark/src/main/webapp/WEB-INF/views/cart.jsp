<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cart List</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="menu.jsp"></jsp:include>
<h3>Cart</h3>
<c:if test="${empty errorString}">
<table border="1" cellpadding="5" cellspacing="1" >
        <th>Code of bought products</th>
        <th>Count</th>
        <th>Price</th>
    <c:forEach items="${cart}" var="cart" >
    <tr>
        <td><c:out value="${cart.title}"/></td>
        <td><c:out value="${cart.count}"/></td>
        <td><c:out value="${cart.price}"/></td>
    </tr>
    </c:forEach>

</table>
<th>amount:</th>
<tr>
    <td>${amountA}</td>
</tr>
    <%--<a href="cartDelete">Delete</a>--%>
</c:if>
<c:if test="${not empty errorString}">
    <p style="color: red;">${errorString}</p>
</c:if>
</body>
</html>
