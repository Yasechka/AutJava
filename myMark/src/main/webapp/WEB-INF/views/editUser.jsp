<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Edit Product</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="menu.jsp"></jsp:include>
<h3>Edit User</h3>
<c:if test="${not empty user}">
    <form method="POST" action="editUser">
        <input type="hidden" name="id" value="${user.id}" />
        <table border="0">
            <tr>
                <td>Id</td>
                <td style="color:blue;">${user.id}</td>
            </tr>
            <tr>
                <td>First name</td>
                <td><input type="text" name="firstName" value="${user.firstName}" /></td>
            </tr>
            <tr>
                <td>Second name</td>
                <td><input type="text" name="secondName" value="${user.secondName}" /></td>
            </tr>
            <tr>
                <td>Bithday</td>
                <td><input type="text" name="birthday" value="${user.birthday}" /></td>
            </tr>
            <tr>
                <td>Login</td>
                <td><input type="text" name="login" value="${user.login}" /></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="text" name="password" value="${user.password}" /></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><input type="text" name="email" value="${user.email}" /></td>
            </tr>
            <tr>
                <td colspan = "2">
                    <input type="submit" value="Submit" />
                    <a href="${pageContext.request.contextPath}/userInfo">Cancel</a>
                </td>
            </tr>
        </table>
        <p style="color: red;">${errorString}</p>
    </form>
</c:if>
</body>
</html>