<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Delete Product</title>
</head>

<body>

<jsp:include page="../header.jsp"></jsp:include>
<jsp:include page="../menu.jsp"></jsp:include>

<h3>Delete Product</h3>

<%--<a href="productList">Product List</a>--%>
<c:if test="${not empty product}">
<form method="POST" action="deleteProduct">
    <input type="hidden" name="id" value="${product.id}" />
    <table border="0">
        <tr>
            <td>Delete this product?</td>
            <td style="color:blue;">${product.id}</td>
        </tr>
        <tr>
            <td colspan = "2">
                <input type="submit" value="Submit" />
                <a href="${pageContext.request.contextPath}/productList">Cancel</a>
            </td>
        </tr>
    </table>
    <p style="color: red;">${errorString}</p>
</form>
</c:if>
</body>
</html>