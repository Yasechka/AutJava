<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Edit ProductGr</title>
</head>
<body>
<jsp:include page="../header.jsp"></jsp:include>
<jsp:include page="../menu.jsp"></jsp:include>
<h3>Edit ProductGr</h3>
<c:if test="${not empty productgroup}">
    <form method="POST" action="editProductGr">
        <input type="hidden" name="id" value="${productgroup.id}" />
        <table border="0">
            <tr>
                <td>Id</td>
                <td style="color:blue;">${productgroup.id}</td>
            </tr>
            <tr>
                <td>Title</td>
                <td><input type="text" name="title" value="${productgroup.title}" /></td>
            </tr>
            <tr>
                <td>Description</td>
                <td><input type="text" name="description" value="${productgroup.description}" /></td>
            </tr>
            <tr>
                <td colspan = "2">
                    <input type="submit" value="Submit" />
                    <a href="${pageContext.request.contextPath}/productGrList">Cancel</a>
                </td>
            </tr>
        </table>
        <p style="color: red;">${errorString}</p>
    </form>
</c:if>
</body>
</html>