<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Edit Product</title>
</head>
<body>
<jsp:include page="../header.jsp"></jsp:include>
<jsp:include page="../menu.jsp"></jsp:include>
<h3>Edit Product</h3>
<c:if test="${not empty product}">
    <form method="POST" action="editProduct">
        <input type="hidden" name="id" value="${product.id}" />
        <table border="0">
            <tr>
                <td>Id</td>
                <td style="color:blue;">${product.id}</td>
            </tr>
            <tr>
                <td>Title</td>
                <td><input type="text" name="title" value="${product.title}" /></td>
            </tr>
            <tr>
                <td>Description</td>
                <td><input type="text" name="description" value="${product.description}" /></td>
            </tr>
            <tr>
                <td>Count</td>
                <td><input type="text" name="count" value="${product.count}" /></td>
            </tr>
            <tr>
                <td>Price</td>
                <td><input type="text" name="price" value="${product.price}" /></td>
            </tr>
            <tr>
                <td colspan = "2">
                    <input type="submit" value="Submit" />
                    <a href="${pageContext.request.contextPath}/productList">Cancel</a>
                </td>
            </tr>
        </table>
        <p style="color: red;">${errorString}</p>
    </form>
</c:if>
</body>
</html>