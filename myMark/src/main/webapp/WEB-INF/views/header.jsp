<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div style="float: right; padding: 10px; text-align: right;">

    <c:if test="${loginedUser.login != null}">
        Hello,
        <a href="${pageContext.request.contextPath}/userInfo">
                ${loginedUser.login} </a>
        &nbsp;|&nbsp;
        <a href="${pageContext.request.contextPath}/logout">Logout</a>
    </c:if>
    <c:if test="${loginedUser.login == null}">
        <a href="${pageContext.request.contextPath}/doLogin">Login</a>
        &nbsp;|&nbsp;
        <a href="${pageContext.request.contextPath}/doRegister">Register</a>
    </c:if>

</div>