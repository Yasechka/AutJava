<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Product List</title>
</head>
<body>

<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="menu.jsp"></jsp:include>

<h3>Product List</h3>

<p style="color: red;">${errorString}</p>

<table border="1" cellpadding="5" cellspacing="1" >
    <tr>
        <th>Title</th>
        <th>Desc</th>
        <th>Count</th>
        <th>Price</th>
<c:if test="${userInSession.equals('isAdmin')}">
        <th>Edit</th>
        <th>Delete</th>
</c:if>
    </tr>
    <c:forEach items="${productList}" var="product" >
        <tr>
            <td>${product.title}</td>
            <td>${product.description}</td>
            <td>${product.count}</td>
            <td>${product.price}</td>
            <c:if test="${userInSession.equals('isAdmin')}">
            <td>
                <a href="admin/editProduct?id=${product.id}">Edit</a>
            </td>
            <td>
                <a href="admin/deleteProduct?id=${product.id}">Delete</a>
            </td>
            </c:if>
            <c:if test="${userInSession.equals('isUser')}">
            <td>
                <form method="POST" action="buyPr">
                    <button type="submit" name="id" value="${product.id}">Buy</button>
                    <%--<input type="SUBMIT" name="id" value="${product.id}">--%>
                </form>
            </td>
            </c:if>
        </tr>
    </c:forEach>
    <c:if test="${userInSession.equals('isAdmin')}">
        <a href="/admin/createProduct">Create Product</a><br/>
        <%--|--%>
        <%--<a href="/admin/productGrList">ProductGr List</a><br/>--%>
        <%--${pageContext.request.contextPath}--%>
        <br/>
    </c:if>
</table>
</body>
</html>
