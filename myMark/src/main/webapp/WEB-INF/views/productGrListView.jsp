<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ProductGr List</title>
</head>
<body>

<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="menu.jsp"></jsp:include>

<h3>ProductGr List</h3>

<p style="color: red;">${errorString}</p>

<table border="1" cellpadding="5" cellspacing="1" >
    <tr>
        <th>Title</th>
        <th>Desc</th>
<c:if test="${userInSession.equals('isAdmin')}">
        <th>Edit</th>
        <th>Delete</th>
</c:if>
    </tr>
    <c:forEach items="${productGroupList}" var="productgroup" >
        <tr>
            <td>${productgroup.title}</td>
            <td>${productgroup.description}</td>
            <c:if test="${userInSession.equals('isAdmin')}">
            <td>
                <a href="admin/editProductGr?id=${productgroup.id}">Edit</a>
            </td>
            <td>
                <a href="admin/deleteProductGr?id=${productgroup.id}">Delete</a>
            </td>
            </c:if>
            <c:if test="${userInSession.equals('isUser')}">
                <td>
                    <form method="POST" action="buyPr">
                        <button type="submit" name="id" value="${productgroup.id}">Buy</button>
                        <%--<input type="SUBMIT" name="id" value="${productgroup.id}">--%>
                    </form>
                </td>
            </c:if>
        </tr>
    </c:forEach>
    <c:if test="${userInSession.equals('isAdmin')}">
        <a href="/admin/createProductGr">Create ProductGr</a><br/>
        <%--|--%>
        <%--<a href="/admin/ppgList">Add Product to ProductGr</a><br/>--%>
        <%--${pageContext.request.contextPath}--%>
        <br/>
    </c:if>
</table>
</body>
</html>
