<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Create Product</title>
</head>
<body>

<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="menu.jsp"></jsp:include>

<h3>Registration</h3>

<p style="color: red;">${errorString}</p>

<form method="POST" action="doRegister">
    <table border="0">
        <tr>
            <td>Name</td>
            <td><input type="text" name="firstName" value="${users.name}" /></td>
        </tr>
        <tr>
            <td>Sname</td>
            <td><input type="text" name="secondName" value="${users.surname}" /></td>
        </tr>
        <tr>
            <td>Birthday</td>
            <td><input type="text" name="birthday" value="${users.birthday}"/> format (yyyy-MM-dd)</td>
        </tr>
        <tr>
            <td>Login</td>
            <td><input type="text" name="login" value="${users.login}" /></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="text" name="password" value="${users.password}" /></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><input type="text" name="email" value="${users.email}" /></td>
        </tr>
        <tr>
            <td>Gender</td>
            <td>
                <input type="radio" checked name="sex" value="MALE">Male
                <input type="radio" name="sex" value="FEMALE">Female
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Submit" />
                <a href="home">Cancel</a>
            </td>
        </tr>
    </table>
</form>

</body>
</html>